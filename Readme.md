# ShoppingList service

[![coverage](https://gitlab.com/ruimaciel/shoppinglist/badges/master/coverage.svg?job=coverage)](https://gitlab.com/ruimaciel/shoppinglist/badges/master/coverage.svg?job=coverage)

ShoppingList is a web service developed with ASP.NET Core 3.1.

## Technologies used

### Dependencies

* ASP.NET Core 3.1

* Entity Framework Core

* MediatR

* AutoMapper

* FluentValidation

* Maciel.Monads

* Swashbuckle

### Test dependencies

* xUnit

* Moq

* coverlet

### Build dependencies

* GitLab CICD

* Docker
