﻿using Microsoft.EntityFrameworkCore;
using ShoppingList.Persistence.Configurations;

namespace ShoppingList.Persistence {
    public class ShoppingListDbContext : DbContext {

        public ShoppingListDbContext (DbContextOptions<ShoppingListDbContext> options) 
            : base (options) 
        {
            this.Database.EnsureCreated();
        }

        public DbSet<Domain.Entities.Product> Products {get; set; }
        public DbSet<Domain.Entities.ShoppingTrip> ShoppingTrips {get; set; }
        public DbSet<Domain.Entities.ShoppingTripsProductsRelationship> ShoppingTripsProductsRelationship {get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder) 
        {
            modelBuilder
                .ApplyConfiguration(new ProductConfiguration())
                .ApplyConfiguration(new ShoppingTripConfiguration())
                .ApplyConfiguration(new ShoppingTripsProductsRelationshipConfiguration());
        }
    }
}