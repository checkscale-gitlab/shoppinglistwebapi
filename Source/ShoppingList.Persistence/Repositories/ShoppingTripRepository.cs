using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Maciel.Monads;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Domain.Entities;

namespace ShoppingList.Persistence.Repositories
{
    public class ShoppingTripRepository : BaseRepository<ShoppingTrip, int>, IShoppingTripRepository
    {
        public ShoppingTripRepository(ShoppingListDbContext dbContext) : base(dbContext)
        { }

        public new async Task<Result<ErrorCode, List<ShoppingTrip>>> GetAll(CancellationToken cancellationToken = default)
        {
            var entities = await _dbContext.Set<ShoppingTrip>()
                .OrderBy(e => e.ShoppingTripId)
                .ToListAsync();

            if (entities == null)
            {
                entities = new List<ShoppingTrip>();
            }

            return new Success<List<ShoppingTrip>>(entities);
        }

    }
}
