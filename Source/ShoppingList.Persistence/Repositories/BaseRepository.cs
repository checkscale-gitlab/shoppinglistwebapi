using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Maciel.Monads;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Application.Interfaces.Repositories;

namespace ShoppingList.Persistence.Repositories
{
    public class BaseRepository<TEntity, TId> : IRepository<TEntity, TId> where TEntity : class
    {
        protected ShoppingListDbContext _dbContext;

        public BaseRepository(ShoppingListDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Result<ErrorCode, TEntity>> Create(TEntity entity, CancellationToken token)
        {
            var e = await _dbContext.Set<TEntity>().AddAsync(entity, token);
            await _dbContext.SaveChangesAsync(token);

            return new Success<TEntity>(e.Entity);
        }

        public async Task<Result<ErrorCode, Nothing>> Delete(TId id, CancellationToken token)
        {
            var response = await FindById(id, token);
            if(response.IsFailure)
            {
                return new Failure<ErrorCode>(response.Failure.Value);
            }

            var entity = response.Success.Value;

            _dbContext.Set<TEntity>().Remove(entity);

            await _dbContext.SaveChangesAsync(token);

            return new Success<Nothing>(new Nothing());
        }

        public async Task<Result<ErrorCode, List<TEntity>>> GetAll(CancellationToken cancellationToken = default)
        {
            var entities = await _dbContext.Set<TEntity>().ToListAsync();

            if(entities == null)
            {
                entities = new List<TEntity>();
            }

            return new Success<List<TEntity>>(entities);
        }

        public async Task<Result<ErrorCode, TEntity>> FindById(TId id, CancellationToken token)
        {
            var entity = await _dbContext.Set<TEntity>()
                .FindAsync(keyValues: new object[] { id }, cancellationToken: token);

            if(entity == null)
            {
                return new Failure<ErrorCode>(ErrorCode.NotFound);
            }

            return new Success<TEntity>(entity);
        }

        public async Task<Result<ErrorCode, TEntity>> Update(TEntity entity, CancellationToken token)
        {
            var updatedEntity = _dbContext.Set<TEntity>().Update(entity);

            await _dbContext.SaveChangesAsync(token);

            return new Success<TEntity>(updatedEntity.Entity);
        }
    }
}
