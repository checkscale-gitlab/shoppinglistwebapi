﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShoppingList.Persistence.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShoppingTrips",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingTrips", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    ShoppingTripId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_ShoppingTrips_ShoppingTripId",
                        column: x => x.ShoppingTripId,
                        principalTable: "ShoppingTrips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ShoppingTripsProductsRelationship",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false),
                    ShoppingTripId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingTripsProductsRelationship", x => new { x.ProductId, x.ShoppingTripId });
                    table.ForeignKey(
                        name: "FK_ShoppingTripsProductsRelationship_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShoppingTripsProductsRelationship_ShoppingTrips_ShoppingTripId",
                        column: x => x.ShoppingTripId,
                        principalTable: "ShoppingTrips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShoppingTripId",
                table: "Products",
                column: "ShoppingTripId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingTripsProductsRelationship_ShoppingTripId",
                table: "ShoppingTripsProductsRelationship",
                column: "ShoppingTripId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShoppingTripsProductsRelationship");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "ShoppingTrips");
        }
    }
}
