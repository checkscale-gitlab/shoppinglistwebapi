using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Persistence.Repositories;

namespace ShoppingList.Persistence
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddPersistenceLayer(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsAction )
        {
            services.AddDbContext<ShoppingListDbContext>(
                optionsAction: optionsAction
            );

            services.AddScoped<DbContext, ShoppingListDbContext>();

            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IShoppingTripRepository, ShoppingTripRepository>();

            return services;
        }
    }
}