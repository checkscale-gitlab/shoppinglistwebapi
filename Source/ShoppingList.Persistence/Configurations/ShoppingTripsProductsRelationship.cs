using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShoppingList.Domain.Entities;

namespace ShoppingList.Persistence.Configurations
{
    public class ShoppingTripsProductsRelationshipConfiguration : IEntityTypeConfiguration<Domain.Entities.ShoppingTripsProductsRelationship>
    {
        public void Configure(EntityTypeBuilder<ShoppingTripsProductsRelationship> builder)
        {
            builder.HasKey(e => new {e.ProductId, e.ShoppingTripId});

            builder
                .HasOne(e => e.Product)
                .WithMany()
                .HasForeignKey(e => e.ProductId);
                
            builder
                .HasOne(e => e.ShoppingTrip)
                .WithMany()
                .HasForeignKey(e => e.ShoppingTripId);
                
        }
    }
}