using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShoppingList.Domain.Entities;

namespace ShoppingList.Persistence.Configurations
{
    public class ShoppingTripConfiguration : IEntityTypeConfiguration<Domain.Entities.ShoppingTrip>
    {
        public void Configure(EntityTypeBuilder<ShoppingTrip> builder)
        {
            builder.HasKey(e => e.ShoppingTripId);

            builder.Property(e => e.ShoppingTripId)
                .ValueGeneratedOnAdd();
        }

    }
}
