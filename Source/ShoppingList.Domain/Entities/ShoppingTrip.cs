using System;
using System.Collections.Generic;

namespace ShoppingList.Domain.Entities
{
    public class ShoppingTrip
    {
        public int ShoppingTripId { get; set; }

        public DateTime Date { get; set; }

        public ICollection<Product> Products { get; set; } = new List<Product>();
    }
}
