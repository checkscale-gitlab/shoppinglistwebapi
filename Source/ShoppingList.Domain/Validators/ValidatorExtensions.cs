using FluentValidation;

namespace ShoppingList.Domain.Validators
{
    public class ValidatorExtensions
    {

    }
    // Custom extensions class
    public static class MyExtensions
    {

        // Generic type is hard-coded to string, menaing this method will only appear for string properties.
        // If you wanted it to appear for *all* properties, introduce a second type-parameter called TProperty
        public static IRuleBuilderOptions<T, string> IsValidProductName<T>(this IRuleBuilder<T, string> rule)
        {
            return rule
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(32);
        }
    }
}
