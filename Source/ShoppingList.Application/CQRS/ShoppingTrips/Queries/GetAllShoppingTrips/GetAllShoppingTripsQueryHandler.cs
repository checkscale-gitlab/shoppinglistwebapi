using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Maciel.Monads;
using MediatR;
using ShoppingList.Application.Interfaces.Repositories;

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{

    using QueryResult = Result<ErrorCode, List<ShoppingTripDto>>;
    using OutputType = List<ShoppingTripDto>;

    public class GetAllShoppingTripsQueryHandler : IRequestHandler<GetAllShoppingTripsQuery, QueryResult>
    {
        private readonly IShoppingTripRepository m_repository;
        private readonly IMapper m_mapper;

        public GetAllShoppingTripsQueryHandler(IShoppingTripRepository repository, IMapper mapper)
        {
            m_repository = repository;
            m_mapper = mapper;
        }

        public async Task<QueryResult> Handle(GetAllShoppingTripsQuery request, CancellationToken cancellationToken)
        {
            var result = await m_repository.GetAll(cancellationToken);

            if (result.IsFailure)
            {
                return QueryResult.MakeFailure(ErrorCode.Unknown);
            }

            var shoppingTripEntityList = result.Success.Value;

            var shoppingTripList = m_mapper.Map<OutputType>(shoppingTripEntityList);

            return QueryResult.MakeSuccess(shoppingTripList);
        }
    }
}
