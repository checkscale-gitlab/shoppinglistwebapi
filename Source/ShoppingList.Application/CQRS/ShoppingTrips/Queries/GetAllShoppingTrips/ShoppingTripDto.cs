using System;
using System.Collections.Generic;
using Maciel.Monads;

namespace ShoppingList.Application.CQRS.ShoppingTrips.Queries.GetAllShoppingTrips
{
    public class ShoppingTripDto
    { 
        public DateTime Date { get; set; }

        public ICollection<ProductDto> Products { get; set; }
    }
}
