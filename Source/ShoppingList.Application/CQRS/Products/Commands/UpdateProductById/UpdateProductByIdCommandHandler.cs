using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Maciel.Monads;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ShoppingList.Application.CQRS.Products.Commands.UpdateProductById
{
    using CommandResult = Result<ExitCode, int>;

    public class UpdateProductByIdCommandHandler : IRequestHandler<UpdateProductByIdCommand, CommandResult>
    {
        private readonly DbContext m_context;
        private readonly IMapper m_mapper;

        public UpdateProductByIdCommandHandler(DbContext context, IMapper mapper)
        {
            m_context = context;
            m_mapper = mapper;
        }

        public async Task<CommandResult> Handle(UpdateProductByIdCommand request, CancellationToken cancellationToken)
        {
            // validate command
            var validationResult = new UpdateProductByIdCommandValidator().Validate(request);
            if (validationResult.IsValid)
            {
                return CommandResult.MakeFailure(ExitCode.InvalidCommand);
            }

            // dirty hack to avoid this bug in FindAsync():
            // https://github.com/aspnet/EntityFrameworkCore/issues/12012
            var entity = await m_context.Set<Domain.Entities.Product>().FindAsync(keyValues: new object[] { request.ProductId }, cancellationToken: cancellationToken);
            if (entity == null)
            {
                return CommandResult.MakeFailure(ExitCode.NotFound);
            }

            // update old entity
            entity.Name = request.Name;

            m_context.Entry<Domain.Entities.Product>(entity).State = EntityState.Modified;
            await m_context.SaveChangesAsync(cancellationToken);

            return CommandResult.MakeSuccess(entity.Id);
        }
    }

}
