using Maciel.Monads;
using MediatR;

namespace ShoppingList.Application.CQRS.Products.Commands.UpdateProductById
{
    using CommandResult = Result<ExitCode, int>;

    public enum ExitCode : byte
    {
        InvalidCommand,
        NotFound,
        Unknown
    }

    public class UpdateProductByIdCommand : IRequest<CommandResult>
    {
        public int ProductId { get; set; }

        public string Name { get; set; }
    }
}
