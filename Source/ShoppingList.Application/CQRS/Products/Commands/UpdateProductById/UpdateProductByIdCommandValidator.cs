using FluentValidation;
using Maciel.Monads;
using ShoppingList.Domain.Validators;

namespace ShoppingList.Application.CQRS.Products.Commands.UpdateProductById
{
    public class UpdateProductByIdCommandValidator : AbstractValidator<UpdateProductByIdCommand>
    {
        public UpdateProductByIdCommandValidator()
        {
            RuleFor(x => x.Name).IsValidProductName();
        }
    }

}
