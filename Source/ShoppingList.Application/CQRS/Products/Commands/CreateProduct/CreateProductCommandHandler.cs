using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Maciel.Monads;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ShoppingList.Application.Interfaces.Repositories;

namespace ShoppingList.Application.CQRS.Products.Commands.CreateProduct
{
    using CommandResult = Result<ExitCode, int>;

    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, CommandResult>
    {
        private readonly IProductRepository m_repository;
        private readonly IMapper m_mapper;

        public CreateProductCommandHandler(IProductRepository repository, IMapper mapper)
        {
            m_repository = repository;
            m_mapper = mapper;
        }

        public async Task<CommandResult> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            // validate command
            var validationResult = new CreateProductCommandValidator().Validate(request);
            if (!validationResult.IsValid)
            {
                return CommandResult.MakeFailure(ExitCode.InvalidCommand);
            }

            // let's run the command
            var newEntity = new Domain.Entities.Product { Name = request.Name };

            var response = await m_repository.Create(newEntity, cancellationToken);

            if(response.IsFailure)
            {
                CommandResult.MakeFailure(ExitCode.Unknown);
            }

            return CommandResult.MakeSuccess(response.Success.Value.Id);
        }
    }
}
