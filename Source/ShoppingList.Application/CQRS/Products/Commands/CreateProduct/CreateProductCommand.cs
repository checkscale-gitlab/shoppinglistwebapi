using Maciel.Monads;
using MediatR;

namespace ShoppingList.Application.CQRS.Products.Commands.CreateProduct
{
    using CommandResult = Result<ExitCode, int>;

    public enum ExitCode : byte
    {
        InvalidCommand,
        ProductAlreadyExists,
        Unknown
    }

    public class CreateProductCommand : IRequest<CommandResult>
    {
        public string Name { get; set; }
    }
}
