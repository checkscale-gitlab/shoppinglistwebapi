using FluentValidation;
using ShoppingList.Domain.Validators;

namespace ShoppingList.Application.CQRS.Products.Commands.CreateProduct
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        public CreateProductCommandValidator()
        {
            RuleFor(x => x.Name).IsValidProductName();
        }
    }
}
