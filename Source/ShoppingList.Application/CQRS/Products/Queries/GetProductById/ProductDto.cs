using Maciel.Monads;

namespace ShoppingList.Application.CQRS.Products.Queries.GetProductById
{
    public class ProductDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
