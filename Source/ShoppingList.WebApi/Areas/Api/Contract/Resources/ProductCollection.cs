using System.Collections.Generic;

namespace ShoppingList.WebUI.Areas.Api.Contract.Resources
{
    public class ProductCollection
    {
        public List<Product> Products {get; set; } = new List<Product>();
    }
}