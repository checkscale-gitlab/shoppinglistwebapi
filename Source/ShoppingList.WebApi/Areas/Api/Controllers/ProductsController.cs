﻿using System.Threading.Tasks;
using AutoMapper;
using Halcyon.HAL;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CQRS = ShoppingList.Application.CQRS;
using ShoppingList.Application.CQRS.Products.Commands.CreateProduct;
using ShoppingList.Application.CQRS.Products.Commands.DeleteProductById;
using ShoppingList.Application.CQRS.Products.Commands.UpdateProductById;
using ShoppingList.Application.CQRS.Products.Queries.GetAllProducts;
using ShoppingList.Application.CQRS.Products.Queries.GetProductById;

using Resources = ShoppingList.WebUI.Areas.Api.Contract.Resources;
using RequestDocuments = ShoppingList.WebUI.Areas.Api.Contract.RequestDocuments;

namespace ShoppingList.WebUI.Areas.Api.Controllers
{

    [Authorize]
    [ApiController]
    [Area("api")]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {

        private readonly IMapper m_mapper;
        private readonly IMediator m_mediator;

        public ProductsController(IMapper mapper, IMediator mediator)
        {
            m_mapper = mapper;
            m_mediator = mediator;
        }

        [HttpGet("", Name = nameof(GetProductCollection))]
        [ProducesResponseType(typeof(Resources.ProductCollection), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductCollection()
        {
            var query = new GetAllProductsQuery();
            var response = await m_mediator.Send(query);

            if (response.IsFailure)
            {
                return NotFound();
            }

            var resource = m_mapper.Map<Resources.ProductCollection>(response.Success.Value);

            // output JSON HAL
            var halResource = new HALResponse(resource)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", Request.Path),
                            new Link("product", "/Products/{productId}", replaceParameters: false),
                            new Link("up", "/")
                    }
                );

            return Ok(value: halResource);
        }

        [HttpGet("{productId:int}", Name = nameof(GetProductById))]
        [ProducesResponseType(typeof(Resources.Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductById([FromRoute] int productId)
        {
            var query = new GetProductByIdQuery { ProductId = productId };
            var response = await m_mediator.Send(query);

            if (response.IsFailure)
            {
                return NotFound();
            }

            var resource = m_mapper.Map<Resources.Product>(response.Success.Value);

            // assemble the HAL resource
            var halResource = new HALResponse(resource)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", Request.Path),
                            new Link("up", "/Products")
                    }
                );

            return Ok(value: halResource);
        }

        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateProduct([FromBody] RequestDocuments.Product product)
        {
            var command = m_mapper.Map<CreateProductCommand>(product);
            var response = await m_mediator.Send(command);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case CQRS.Products.Commands.CreateProduct.ExitCode.ProductAlreadyExists:
                        return Conflict();

                    case CQRS.Products.Commands.CreateProduct.ExitCode.Unknown:
                    default:
                        return StatusCode(StatusCodes.Status500InternalServerError);
                }
            }

            return CreatedAtAction(actionName: nameof(GetProductById), routeValues: new { productId = response.Success.Value }, value: null);
        }

        [HttpPut("{productId:int}")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateProductById([FromRoute] int productId, [FromBody] RequestDocuments.Product request)
        {
            var command = m_mapper.Map<UpdateProductByIdCommand>(request);
            command.ProductId = productId;
            var response = await m_mediator.Send(command);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case CQRS.Products.Commands.UpdateProductById.ExitCode.NotFound:
                        return NotFound();

                    case CQRS.Products.Commands.UpdateProductById.ExitCode.Unknown:
                    default:
                        return StatusCode(500);
                }
            }

            return Ok();
        }

        [HttpDelete("{productId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteProductById([FromRoute] int productId)
        {
            var command = new DeleteProductByIdCommand { ProductId = productId };

            var response = await m_mediator.Send(command);

            if (response.IsFailure)
            {
                switch (response.Failure.Value)
                {
                    case CQRS.Products.Commands.DeleteProductById.ErrorCode.NotFound:
                        return NotFound();

                    default:
                        return BadRequest();
                }
            }

            return NoContent();
        }

    }
}
