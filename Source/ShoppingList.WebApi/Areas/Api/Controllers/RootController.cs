using System.Threading.Tasks;
using Halcyon.HAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace ShoppingList.WebUI.Areas.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Area("api")]
    [Route("")]
    public class RootController : ControllerBase
    {

        protected readonly LinkGenerator m_linkgen;

        public RootController(LinkGenerator linken)
        {
            m_linkgen = linken;
        }

        [HttpGet]
        [Produces("application/hal+json")]
        public async Task<IActionResult> Get()
        {
            var response = new HALResponse(null)
                .AddLinks(
                    new Link[]
                    {
                        new Link("self", Request.Path),
                            new Link("curie", UriHelper.GetDisplayUrl(Request)) { Name = "sl" },
                            new Link("sl:products", m_linkgen.GetPathByAction(HttpContext, action : nameof(ProductsController.GetProductCollection), controller: "Products")),
                            new Link("sl:shoppingtrips", m_linkgen.GetPathByAction(HttpContext, action : nameof(ShoppingTripsController.GetShoppingTripCollection), controller: "ShoppingTrips"))
                    }
                );

            return await Task.FromResult<IActionResult>(Ok(response));
        }

    }
}
