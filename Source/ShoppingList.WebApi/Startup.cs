﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ShoppingList.Application.CQRS.Products.Commands.CreateProduct;
using ShoppingList.Persistence;

namespace ShoppingList.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void StartupConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(Application.CQRS.Products.Queries.GetProductById.GetProductByIdQuery).GetTypeInfo().Assembly);

            services.AddTransient<IValidator<Areas.Api.Contract.RequestDocuments.Product>, Areas.Api.Contract.RequestDocuments.ProductValidator>();

            services.AddAutoMapper(
                typeof(Areas.Api.Contract.Resources.MappingProfile),
                typeof(Application.Infrastructure.AutoMapperProfile)
            );

            services.AddHealthChecks()
                .AddDbContextCheck<ShoppingListDbContext>();

            services.Configure<Options.Postgres>(Configuration.GetSection("Postgres"));

            var postgresOptions = services.BuildServiceProvider().GetService<IOptions<Options.Postgres>>().Value;
            services.AddPersistenceLayer(
                optionsAction: options => options.UseNpgsql(postgresOptions.ConnectionString())
            );
        }

        public void ConfigureServices(IServiceCollection services)
        {

        }

        public void ConfigureDevelopmentServices(IServiceCollection services)
        {
            StartupConfigureServices(services);

            services
                .AddControllersWithViews()
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<Program>();
                    fv.RegisterValidatorsFromAssemblyContaining<CreateProductCommandValidator>();
                });
        }

        public void ConfigureStagingServices(IServiceCollection services)
        {
            StartupConfigureServices(services);

            services
                .AddControllersWithViews()
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<Program>();
                    fv.RegisterValidatorsFromAssemblyContaining<CreateProductCommandValidator>();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://maciel.eu.auth0.com/";
                options.Audience = "https://api.shoppinglist.maciel.site";
            });
        }

        public void ConfigureProductionServices(IServiceCollection services)
        {
            ConfigureStagingServices(services);
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseRouting();

            app.UseAuthentication();

            app.UseHealthChecks("/health", new HealthCheckOptions()
            {
                AllowCachingResponses = false
            });
            app.UseHttpsRedirection();
        }
    }
}
