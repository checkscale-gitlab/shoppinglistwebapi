namespace ShoppingList.WebUI.Options
{
    public class Postgres
    {
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 5432;
        public string Database { get; set; } = "shoppinglist";
        public string Username { get; set; } = "postgres";
        public string Password { get; set; } = "postgres";

        public string ConnectionString()
        {
            // https://www.npgsql.org/doc/connection-string-parameters.html

            return $"Host={Host}; Port={Port}; Database={Database}; Username={Username}; Password={Password};";
        }

    }
}
