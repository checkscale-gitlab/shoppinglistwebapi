FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

WORKDIR /app

ADD Source Source
ADD Tests Tests
ADD ShoppingList.sln .
RUN dotnet restore
RUN dotnet publish Source/ShoppingList.WebApi -c Release -o /app/out
ENTRYPOINT ["bash"]

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1

LABEL org.label-schema.schema-version="1.0" \
    org.label-schema.name="ShoppingList" \
    org.label-schema.description="A small example of a Web API implemented in ASP.NET Core 3.1 app" \
    org.label-schema.url="https://gitlab.com/ruimaciel/shoppinglist" \
    org.label-schema.vcs-url="git@gitlab.com:ruimaciel/shoppinglist.git"

ARG ASPNETCORE_ENVIRONMENT
ENV ASPNETCORE_ENVIRONMENT=${ASPNETCORE_ENVIRONMENT:-Development} \
    SHOPPINGLIST_POSTGRES__Host=${SHOPPINGLIST_POSTGRES__Host} \
    SHOPPINGLIST_POSTGRES__Database=${SHOPPINGLIST_POSTGRES__Database:-"shoppinglist"} \
    SHOPPINGLIST_POSTGRES__Username=${SHOPPINGLIST_POSTGRES__Username:-"postgres"} \
    SHOPPINGLIST_POSTGRES__Password=${SHOPPINGLIST_POSTGRES__Password:-"postgres"}

EXPOSE 80

WORKDIR /opt/ShoppingList
COPY --from=build-env /app/out .

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 \
    CMD curl -f http://localhost/health || exit 1

ENTRYPOINT ["dotnet", "ShoppingList.WebApi.dll"]
