using ShoppingList.Domain.Entities;
using Xunit;

namespace ShoppingList.Domain.Tests
{
    public class TestProduct
    {
        [Fact]
        public void Test_HappyPath()
        {
            //Given
            var name = "Name";

            //When
            var product = new Product{Name = name};

            //Then
            Assert.Equal(name, product.Name);
        }
    }
}
