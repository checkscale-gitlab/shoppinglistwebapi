using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Application.CQRS.Products.Commands.CreateProduct;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Persistence;
using ShoppingList.Persistence.Repositories;
using Xunit;

namespace ShoppingList.Application.Tests
{
    public class TestCreateProduct
    {

        private IServiceProvider ServiceProvider { get; }

        public TestCreateProduct()
        {
            ServiceProvider = new ServiceCollection()
                .AddScoped<DbContext, ShoppingListDbContext>()
                .AddDbContext<ShoppingListDbContext>(
                    optionsAction: options => options.UseInMemoryDatabase(databaseName: "shopping_list_database")
                )
                .AddAutoMapper(typeof(Application.Infrastructure.AutoMapperProfile).GetTypeInfo().Assembly)
                .AddScoped<IMediator, Mediator>()
                .AddScoped<IProductRepository, ProductRepository>()
                .AddMediatR(typeof(Application.CQRS.Products.Queries.GetProductById.GetProductByIdQueryHandler).GetTypeInfo().Assembly)
                .BuildServiceProvider();
        }

        [Fact]
        public async Task TestHappyPath()
        {
            //Given
            var command = new CreateProductCommand
            {
                Name = "Foo"
            };

            //When
            var mediator = (IMediator)ServiceProvider.GetService(typeof(IMediator));
            var response = await mediator.Send(command);

            //Then
            Assert.True(response.IsSuccess);
        }

        [Fact]
        public async Task Given_CommandWithNoName_When_ApplyCommand_Then_ResponseIsFailure()
        {
            //Given
            var command = new CreateProductCommand
            {
            };

            //When
            var mediator = (IMediator)ServiceProvider.GetService(typeof(IMediator));
            var response = await mediator.Send(command);

            //Then
            Assert.True(response.IsFailure);
        }
    }
}
