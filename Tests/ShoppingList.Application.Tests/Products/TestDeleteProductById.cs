using System;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Application.CQRS.Products.Commands.DeleteProductById;
using ShoppingList.Application.Interfaces.Repositories;
using ShoppingList.Persistence;
using ShoppingList.Persistence.Repositories;
using Xunit;

namespace ShoppingList.Application.Tests
{
    public class TestDeleteProductById
    {

        private IServiceProvider ServiceProvider { get; }
        private IProductRepository ProductRepository { get; set; }
        private IMediator Mediator { get; set; }

        public TestDeleteProductById()
        {
            ServiceProvider = new ServiceCollection()
                .AddScoped<DbContext, ShoppingListDbContext>()
                .AddDbContext<ShoppingListDbContext>(
                    optionsAction: options => options.UseInMemoryDatabase(databaseName: "shopping_list_database")
                )
                .AddAutoMapper(typeof(Application.Infrastructure.AutoMapperProfile).GetTypeInfo().Assembly)
                .AddScoped<IMediator, Mediator>()
                .AddScoped<IProductRepository, ProductRepository>()
                .AddMediatR(typeof(Application.CQRS.Products.Queries.GetProductById.GetProductByIdQueryHandler).GetTypeInfo().Assembly)
                .BuildServiceProvider();

            ProductRepository = ServiceProvider.GetRequiredService<IProductRepository>();
            Mediator = ServiceProvider.GetRequiredService<IMediator>();
        }

        [Fact]
        public async Task Given_EmptyRepository_When_DeleteProductById_Then_ResultIsNotFound()
        {
            //Given
            var command = new DeleteProductByIdCommand { ProductId = 0 };

            //When
            var response = await Mediator.Send(command);

            //Then
            Assert.True(response.IsFailure);
            Assert.Equal(CQRS.Products.Commands.DeleteProductById.ErrorCode.NotFound, response.Failure.Value);
        }

    }
}
