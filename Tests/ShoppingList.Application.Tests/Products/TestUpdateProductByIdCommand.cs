using System;
using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Application.CQRS.Products.Commands.UpdateProductById;
using ShoppingList.Persistence;
using Xunit;

namespace ShoppingList.Application.Tests
{
    public class TestUpdateProductByIdCommand
    {

        private IServiceProvider ServiceProvider { get; }

        public TestUpdateProductByIdCommand()
        {
            ServiceProvider = new ServiceCollection()
                .AddScoped<DbContext, ShoppingListDbContext>()
                .AddDbContext<ShoppingListDbContext>(
                    optionsAction: options => options.UseInMemoryDatabase(databaseName: "shopping_list_database")
                )
                .AddAutoMapper(typeof(Application.Infrastructure.AutoMapperProfile).GetTypeInfo().Assembly)
                .AddScoped<IMediator, Mediator>()
                .AddMediatR(typeof(Application.CQRS.Products.Queries.GetProductById.GetProductByIdQueryHandler).GetTypeInfo().Assembly)
                .BuildServiceProvider();
        }

        [Fact]
        public async System.Threading.Tasks.Task Given_EmptyRepository_When_UpdateProduct_Then_ResponseIsFailure()
        {
            //Given
            var command = new UpdateProductByIdCommand
            {
                ProductId = 1,
                Name = "Foo"
            };

            //When
            var mediator = (IMediator)ServiceProvider.GetService(typeof(IMediator));
            var response = await mediator.Send(command);

            //Then
            Assert.True(response.IsFailure);
        }
        
    }
}